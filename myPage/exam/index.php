<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../bootstrap/css/toastr.min.css">
<link rel="stylesheet" type="text/css" href="../bootstrap/css/style.css">
<?php include 'functions.php'; ?>
</head>
<body>
    <div class="page-header">
      <h1>Create Retrieve Update Delete using phpMyAdmin<br><small>Download source code using GIT. Url here <a href="https://gitlab.com/khert/crud">https://gitlab.com/khert/crud</a> @author: Khert Geverola</small></h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h1 class="text-center login-title"><strong>C</strong>reate</h1>
                <div class="account-wall">
                    <img class="profile-img" src="../bootstrap/pictures/Picture1.png" alt="">
                    <BR>
                    <form class="form-signin" method="post" action="">
                        <input type="text" name="username" id="username" class="form-control" placeholder="Username" required autofocus>
                        <input type="password" name="password" id="password"class="form-control" placeholder="Password" required>
                        <BR>
                        <BR>
                        <input class="btn btn-lg btn-primary btn-block" id="insert" name="insert" type="submit" value="INSERT">   
                    </form>
                </div>
            </div>
            <div class="col-md-3">
                <h1 class="text-center login-title"><strong>R</strong>etrieve</h1>
                <div class="account-wall">
                    <img class="profile-img" src="../bootstrap/pictures/Picture1.png"
                        alt="">
                    <form class="form-signin" method="post" action="">
                         <input name="load" value="LOAD DATA" class="btn btn-lg btn-primary btn-block" type="submit">
                    <ul class="list-group">
                        <?php
                            $conn = new dbConn();
                            if (isset($_POST['load'])) {
                                $conn->printData();
                            }
                         ?>
                    </ul>
                    </form>
                </div>
            </div>
            <div class="col-md-3">
                <h1 class="text-center login-title"><strong>U</strong>pdate</h1>
                <div class="account-wall">
                    <img class="profile-img" src="../bootstrap/pictures/Picture1.png"
                        alt="">
                    <form class="form-signin" method="post" action="">
                         <input type="number" name="id" id="username" class="form-control" placeholder="User id to change" required autofocus><br>
                    <input type="text" name="usernameU" id="usernameU" class="form-control" placeholder="New username" required >
                    <input type="password" name="passwordU" id="passwordU" class="form-control" placeholder="New password" required>
                    <input value="UPDATE" class="btn btn-lg btn-primary btn-block" name="update" type="submit">
                    </form>
                </div>
            </div>
            <div class="col-md-3">
                <h1 class="text-center login-title"><strong>D</strong>elete</h1>
                <div class="account-wall">
                    <img class="profile-img" src="../bootstrap/pictures/Picture1.png"
                        alt="">
                    <form class="form-signin" method="post" action="">
                        <BR><BR><BR>
                    <input type="number" name="id" id="username" class="form-control" placeholder="Input user id to delete" required autofocus><BR><BR><BR>
                    <input value="DELETE" name="delete" class="btn btn-lg btn-primary btn-block" type="submit">                  
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="../bootstrap/js/query.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/toastr.min.js"></script>
<!-- <script src="../bootstrap/js/myScript.js"></script> -->
<?php 
    $conn = new dbConn();
    if (isset($_POST['insert'])) {
        $username =$_POST['username'];
        $password = $_POST['password'];
        $conn->insertDB($username,$password);
    }
    if (isset($_POST['update'])) {
        $id =$_POST['id'];
        $username =$_POST['usernameU'];
        $password = $_POST['passwordU'];
        $conn->update($id,$username,$password);
    }
     if (isset($_POST['delete'])) {
        $id =$_POST['id'];
        $conn->delete($id);
    }
?>
</html>